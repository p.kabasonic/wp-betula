<?php
/**
 * Template Name: Page PDF
 */

require __DIR__ . '/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;

if(isset($_GET['code']) && isset($_GET['hash'])) {

    $code = $_GET['code'];
    $post = get_page_by_title($code, OBJECT, 'register_code');
    if ($post) {

        $hash = get_field('hash', $post->ID);
        if($hash == $_GET['hash']) {
            $myProjectDirectory = get_template_directory();

            $content = array();

            $product_acf = get_field('product', $post->ID);

            $product = wc_get_product($product_acf->ID);

            $price = $product->get_price();

            $prod_tmp = getSimpleProduct($product);

            $terms = get_the_terms($product->get_ID(), 'product_cat');

            $type = '-';
            $strength = '-';
            $body_part = '-';
            if ($terms && !is_wp_error($terms)) {
                $content['categories'] = $terms;
                $i = 0;
                foreach($terms as $term) {
                    if($i === 0) {
                        $type = $term->name;
                    }
                    if($i === 1) {
                        $strength = $term->name;
                    }
                    if($i === 2) {
                        $body_part = $term->name;
                    }
                    $i++;
                }
            }
            $content['product'] = $prod_tmp;

            $time = '-';
            $variation_id = get_field('variant_id', $post->ID);
            if($variation_id != '') {
                $variation = wc_get_product($variation_id);
                $price = $variation->get_price();
                $time = get_post_meta( $variation_id, 'attribute_pa_time', true);
            }


            $content['type'] = $type;
            $content['strength'] = $strength;
            $content['body_part'] = $body_part;
            $content['coupon'] = $post->post_title;
            $content['price'] = $price;
            $content['time'] = $time;
            $content['name'] = $product->get_name();
            $content['product_text'] = $product->get_description();
            $content['footer_img'] = get_template_directory_uri() . '/templates/pdf/footerPDF.png';
            $content['product_img'] = get_template_directory_uri() . '/templates/pdf/product.png';


            $outputHTML = Timber::compile('pdf/pdf-template.twig', $content);

            $html2pdf = new Html2Pdf('P', 'A4', 'PL', true, 'UTF-8', array(0, 0, 0, 0));
            $html2pdf->addFont('dejavusansb', 'B', 'dejavusansb');
            $html2pdf->setDefaultFont("dejavusans");
            $html2pdf->writeHTML($outputHTML);
            $html2pdf->output();

        } else {
            wp_redirect( home_url() );
            exit;
        }
    } else {
        wp_redirect( home_url() );
        exit;
    }
} else {
    wp_redirect( home_url() );
    exit;
}



//Timber::render(array('page' . $post->post_name . '.twig', 'page.twig'), $context);