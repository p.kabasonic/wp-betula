
# Betula

## Titles
* `.h1 - 48/64px bold`
* `.h2 - 36/48px bold`
* `.h3 - 22/36px bold`

## Content
* `.content - 18/30px medium`

## Wrappers

* `.wrapper - max-width: 1040px;`
* `.wrapper-xl - max-width: 1200px;`

## Render images
`{% include 'partial/image.twig' with {'image' : layout.image, 'classes' : ''} %}`

## Render links
`{% include 'partial/link.twig' with {
'link' : layout.link,
'classes' : 'button--green button--icon',
'icon' : 'messenger'
} only %}`

## Global classes
`bottom-green-line` - underline green
`bottom-silver-line` - underline silver

## Buttons

* `.button`
* `.button--green`
* `.button--green--outline`
* `.button--big`
* `.button--icon`
