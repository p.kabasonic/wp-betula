<?php
define("DECIMALS", wc_get_price_decimals());
define("DECIMAL_SEPARATOR", '.');
define("THOUSANDS_SEPARATOR", ' ');

add_action('wp_ajax_get_products_by_category', 'get_products_by_category');
add_action('wp_ajax_nopriv_get_products_by_category', 'get_products_by_category');
function get_products_by_category()
{
    $categoryIds = esc_sql($_POST['ids']);

    $promotion = esc_sql($_POST['promotion']);

    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
    );

    if ($categoryIds) {
        $categories = explode(',', $categoryIds);

        $args['tax_query'] = array(
            'relation' => 'OR',
        );

        foreach ($categories as $cat) {
            array_push($args['tax_query'], array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id',
                'terms' => $cat,
            ));
        }
    }
    if($promotion === "true") {
        $args['meta_query'] = array(
            array(
                'key' => 'promotion',
                'value' => '1',
                'compare' => '=='
            )
        );
    }

    $products = get_posts($args);

    $data['countProducts'] = count($products);
    $data['products'] = getProducts($products);

    wp_send_json_success($data);
}

add_action('wp_ajax_add_to_cart', 'add_to_cart');
add_action('wp_ajax_nopriv_add_to_cart', 'add_to_cart');
function add_to_cart()
{
    global $sitepress;
    global $wpdb;

    $product_id = esc_sql($_POST['id']);
    $quantity = intval(esc_sql($_POST['quantity']));
    $attributes = esc_sql($_POST['attributes']);

    if (!$quantity)
        $quantity = 1;

    global $woocommerce;
    $cart = WC()->instance()->cart;

    if (count($attributes) > 0 && is_array($attributes)) {
        $attr = array();
        foreach ($attributes as $item) {
            $key = strtolower($item['type']);
            $value = $item['value'];
            $attr[$key] = $value;
        }
        //  $attr["attribute_pa_rozmiar"] =  'l-en';
        //  $attr["attribute_pa_wykonczenie"] =  's-en';
        $variation_id = find_matching_product_variation_id($product_id, $attr);

        $addingToCart = $woocommerce->cart->add_to_cart($product_id, $quantity, $variation_id, $attr);
    } else {
        $addingToCart = $woocommerce->cart->add_to_cart($product_id, $quantity);
    }

    if ($addingToCart) {
        $cart->cart = getCart();

        wp_send_json_success($cart);
    } else wp_send_json_error($cart);
}

add_action('wp_ajax_get_cart', 'get_cart');
add_action('wp_ajax_nopriv_get_cart', 'get_cart');
function get_cart()
{
    global $woocommerce;

    $cart = $woocommerce->cart->get_cart();

    if (!$cart) wp_send_json_error($cart);

    $cart->cart = getCart();

    wp_send_json_success($cart);
}

add_action('wp_ajax_remove_from_cart', 'remove_from_cart');
add_action('wp_ajax_nopriv_remove_from_cart', 'remove_from_cart');
function remove_from_cart()
{
    $cart_item_key = esc_sql($_POST['cart_item_key']);

    global $woocommerce;
    $cart = WC()->instance()->cart;

    if ($woocommerce->cart->remove_cart_item($cart_item_key)) {
        $cart->calculate_totals();

        $cart->cart = getCart();
        wp_send_json_success($cart);
    } else wp_send_json_error($cart);
}

add_action('wp_ajax_set_quantity', 'set_quantity');
add_action('wp_ajax_nopriv_set_quantity', 'set_quantity');
function set_quantity()
{
    $cart_item_key = esc_sql($_POST['cart_item_key']);
    $quantity = intval(esc_sql($_POST['quantity']));

    global $woocommerce;
    $cart = WC()->instance()->cart;

    if ($woocommerce->cart->set_quantity($cart_item_key, $quantity)) {
        $cart->cart = getCart();

        wp_send_json_success($cart);
    } else wp_send_json_error($cart);
}

add_action('wp_ajax_check_coupon_code', 'check_coupon_code');
add_action('wp_ajax_nopriv_check_coupon_code', 'check_coupon_code');
function check_coupon_code()
{
    global $woocommerce;
    $coupon_code = esc_sql($_POST['coupon']);

    $coupon = new \WC_Coupon($coupon_code);
    $discounts = new \WC_Discounts(WC()->cart);
    $valid_response = $discounts->is_coupon_valid($coupon);
    if (is_wp_error($valid_response)) {
        $response = "Podaj poprawny kod rabatowy";
        wp_send_json_error($response);
    } else {

        $woocommerce->cart->add_discount($coupon_code);

        WC()->cart->calculate_totals();
        $cart = WC()->instance()->cart;
        $cart->cart = getCart();

        wp_send_json_success($cart);
    }
}

add_action('wp_ajax_remove_coupon_code', 'remove_coupon_code');
add_action('wp_ajax_nopriv_remove_coupon_code', 'remove_coupon_code');
function remove_coupon_code()
{
    global $woocommerce;
    WC()->cart->remove_coupons();
    WC()->cart->calculate_totals();
    $cart = WC()->instance()->cart;
    $cart->cart = getCart();
    wp_send_json_success($cart);
}


function getCart()
{
    global $wpdb, $woocommerce;
    $data = array();

    $cart = $woocommerce->cart->get_cart();

    $data['products'] = array();
    WC()->cart->calculate_totals();

    $tax_total = WC()->cart->get_taxes_total();
    $shipping_total = WC()->cart->get_shipping_total();
    $price_products_total = WC()->cart->get_subtotal();
    $data['tax_total'] = number_format($tax_total, DECIMALS, DECIMAL_SEPARATOR, THOUSANDS_SEPARATOR);
    $data['shipping_total'] = number_format($shipping_total + $tax_total, DECIMALS, DECIMAL_SEPARATOR, THOUSANDS_SEPARATOR);
    $data['price_products_total'] = number_format($price_products_total, DECIMALS, DECIMAL_SEPARATOR, THOUSANDS_SEPARATOR);
    $data['total'] = number_format(WC()->cart->total, DECIMALS, DECIMAL_SEPARATOR, THOUSANDS_SEPARATOR);

    $data['quantity_cart'] = WC()->cart->get_cart_contents_count();
    $data['checkout_url'] = wc_get_checkout_url();
    $data['currency'] = html_entity_decode(get_woocommerce_currency_symbol());
    if (WC()->cart->get_coupons()) {
        $wc_coupon = WC()->cart->get_coupons()[WC()->cart->get_applied_coupons()[0]];
        $data['discount_total'] = number_format(WC()->cart->get_cart_discount_total(), DECIMALS, DECIMAL_SEPARATOR, THOUSANDS_SEPARATOR);
        $data['discount'] = number_format($wc_coupon->get_data()['amount'], DECIMALS, DECIMAL_SEPARATOR, THOUSANDS_SEPARATOR);
        $data['discountType'] = $wc_coupon->get_data()['discount_type'];
        $data['code'] = $wc_coupon->get_data()['code'];
    }
    $data['products'] = getProductsCart($cart);

    return $data;
}

function getProductsCart($products)
{
    $tmpProducts = array();

    foreach ($products as $key => $product) {
        if (!isset($product['bundled_by'])) {
            array_push($tmpProducts, getProduct($product, $key));
        }
    }

    return $tmpProducts;
}

function getProduct($product, $key)
{
    $product_tmp = array();
    $product_tmp['id'] = $product['product_id'];
    $product_tmp['name'] = get_the_title($product['product_id']);

//    if ($product['variation_id'] !== 0) $product_tmp['image'] = get_the_post_thumbnail_url($product['variation_id']);
//    else $product_tmp['image'] = get_the_post_thumbnail_url($product['product_id']);
    $product_tmp['image'] = get_the_post_thumbnail_url($product['product_id']);

    $product_tmp['type'] = get_the_terms($product['product_id'], 'product_cat')[0]->name;
    $product_tmp['tax'] = number_format($product['line_subtotal_tax'] / $product['quantity'], DECIMALS, '.', '');
    $product_tmp['slug'] = $product['data']->get_data()['slug'];
    $product_tmp['url'] = get_permalink($product['product_id']);
    $product_tmp['quantity'] = $product['quantity'];

    if ($key)
        $product_tmp['key'] = $key;

    $product_tmp['price'] = number_format($product['line_subtotal'] + $product['line_subtotal_tax'], DECIMALS, '.', '');

    $variation = array();
    foreach ($product['variation'] as $key_v => $value) {
        $attr_name = substr($key_v, 10);
        $term = get_term_by('slug', $value, $attr_name);
        $key_v = wc_attribute_label($attr_name);
        $variation[$key_v] = $term->name;
    }
    $product_tmp['variation'] = $variation;
    $product_tmp['attributes'] = $product['variation'];

    return $product_tmp;
}

function find_matching_product_variation_id($product_id, $attributes)
{
    return (new \WC_Product_Data_Store_CPT())->find_matching_product_variation(
        new \WC_Product($product_id),
        $attributes
    );
}

add_filter('woocommerce_billing_fields', 'wc_billing_fields', 10, 1);
function wc_billing_fields($fields)
{

    $fields['billing_first_name']['label'] = 'Imię * <span class="text-required">Wymagane</span>';
    $fields['billing_first_name']['placeholder'] = 'Wprowadź imię';
    $fields['billing_first_name']['priority'] = 1;

    $fields['billing_last_name']['label'] = 'Nazwisko * <span class="text-required">Wymagane</span>';
    $fields['billing_last_name']['placeholder'] = 'Wprowadź nazwisko';
    $fields['billing_last_name']['priority'] = 2;

    $fields['billing_email']['label'] = 'Adres email * <span class="text-required">Wymagane</span><span class="text-email">Podaj poprawny email</span>';
    $fields['billing_email']['placeholder'] = 'anna.kowalska@gmail.com';
    $fields['billing_email']['priority'] = 3;

    $fields['billing_address_1']['label'] = 'Ulica';
    $fields['billing_address_1']['placeholder'] = 'Słowackiego';
    $fields['billing_address_1']['required'] = false;
    $fields['billing_address_1']['priority'] = 4;

    unset($fields['billing_company']);
    unset($fields['billing_country']);
    unset($fields['billing_state']);

    $fields['billing_address_2']['label'] = 'Numer budynku, numer mieszkania';
    $fields['billing_address_2']['required'] = false;
    $fields['billing_address_2']['label_class'] = array();
    $fields['billing_address_2']['placeholder'] = '56/3';
    $fields['billing_address_2']['priority'] = 5;

    $fields['billing_postcode']['label'] = 'Kod pocztowy';
    $fields['billing_postcode']['placeholder'] = '31-120';
    $fields['billing_postcode']['required'] = false;
//    $fields['billing_postcode']['validate'] = false;
    $fields['billing_postcode']['priority'] = 6;

    $fields['billing_phone']['label'] = 'Telefon';
    $fields['billing_phone']['placeholder'] = '+48 500 456 789';
    $fields['billing_phone']['required'] = false;
    $fields['billing_phone']['priority'] = 7;

    $fields['billing_city']['label'] = 'Miejscowość';
    $fields['billing_city']['placeholder'] = 'Kraków';
    $fields['billing_city']['required'] = false;
    $fields['billing_city']['priority'] = 8;

    return $fields;
}

add_filter('woocommerce_checkout_fields', 'wc_order_fields', 10, 1);
function wc_order_fields($fields)
{
    $fields['order']['order_comments']['label'] = 'Treść dedykacji <span class="c-optional">(opcjonalnie)</span>';
    $fields['order']['order_comments']['placeholder'] = 'Twoja dedykacja';
    $fields['order']['order_comments']['required'] = false;
    $fields['order']['order_comments']['priority'] = 8;

    return $fields;
}

add_filter( 'woocommerce_terms_is_checked_default', '__return_true' );
<<<<<<< HEAD

add_action('woocommerce_after_order_notes', 'my_custom_checkout_field');
function my_custom_checkout_field($checkout)
{
    woocommerce_form_field('js-checkout-invoice', array(
        'type' => 'checkbox',
        'class' => array('input-checkbox'),
        'label' => __('Chcę otrzymać fakturę VAT <span class="c-optional">(opcjonalnie)</span>'),
        'required' => false,
        'value' => false,
    ), false);
}


add_action('woocommerce_order_status_completed', 'wc_complete_order_status', 10, 3);

function wc_complete_order_status($order_id)
{
    $order = wc_get_order($order_id);
    $post_ids = array();
    $domain = get_site_url();
    $email_content = 'Dziękujemy za złożenie zamówienie, poniżej Twoje kupony do wykorzystania:<br/>';
    foreach ($order->get_items() as $item_id => $item) {
        for ($i = 0; $i < $item->get_quantity(); $i++) {
            $post_id = generate_hash();
            $date = date('Ymd', strtotime('+1 year'));
            $hash = generate_hash_security(24);
            $product_id = $item->get_product_id();
            $variation_id = $item->get_variation_id();
            update_field('is_used', 0, $post_id);
            update_field('expired_date', $date, $post_id);
            update_field('order', $order_id, $post_id);
            update_field('product', $product_id, $post_id);
            update_field('variant_id', $variation_id, $post_id);
            update_field('hash', $hash, $post_id);
            array_push($post_ids, $post_id);
            $email_content .=  '<a href="'.$domain.'/pdf?code='.get_the_title($post_id).'&hash='.$hash.'">Kod kuponu: '.get_the_title($post_id).'</a><br/>';
        }
    }
   // update_field('register_codes', $post_ids, $order_id);

    $subject = 'Kupony Betula';
    $to = $order->get_billing_email();
    $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Kupony Betula <noreply@' . $domain . '>', 'Reply-To: <' . $to . '>');
    wp_mail($to, $subject, $email_content, $headers);


}


function generate_hash()
{
    $hash = md5(uniqid(rand(), true));
    $value = substr($hash, 0, 8);
    $post = get_page_by_title($value, OBJECT, 'register_code');
    if ($post) {
        generate_hash();
    } else {
        $code = array(
            'post_title' => $value,
            'post_type' => 'register_code',
            'post_status' => 'publish'
        );
        $new_code = wp_insert_post($code);
        return $new_code;
    }
}
function generate_hash_security($count = 20) {
    $hash = md5(uniqid(rand(), true));
    $value = substr($hash, 0, $count);
    return $value;
}

add_action('restrict_manage_posts', 'wpse45436_admin_posts_filter_restrict_manage_posts');

function wpse45436_admin_posts_filter_restrict_manage_posts()
{
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    //add filter to the post type you want
    if ('register_code' == $type) { //Replace NAME_OF_YOUR_POST with the name of custom post
        $values = array(
            'wykorzystany' => 1, //Replace label1 with name and value1 with the value of custom field
            'nie wykorzystany' => 0, //Replace label2 with name and value2 with another value of custom field
        );
        ?>
        <select name="is_used">
            <option value=""><?php _e('Czy kod wykorzystany', 'wose45436'); ?></option>
            <?php $current_v = isset($_GET['is_used']) ? $_GET['is_used'] : '';
            foreach ($values as $label => $value) {
                printf
                (
                    '<option value="%s"%s>%s</option>',
                    $value,
                    $value == $current_v ? ' selected="selected"' : '',
                    $label
                );
            }
            ?>
        </select>
    <?php }
}

/** if submitted filter by post meta */
add_filter('parse_query', 'wpse45436_posts_filter');
function wpse45436_posts_filter($query)
{
    global $pagenow;
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    } //Replace NAME_OF_YOUR_POST with the name of custom post
    if ( 'register_code' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['is_used']) && $_GET['is_used'] != '') {
        $query->query_vars['meta_key'] = 'is_used';
        $query->query_vars['meta_value'] = $_GET['is_used'];
    }
}

add_filter('manage_edit-register_code_columns', 'MY_COLUMNS_FUNCTION');
function MY_COLUMNS_FUNCTION($columns)
{
    $new_columns = (is_array($columns)) ? $columns : array();
    unset($new_columns['order_actions']);

    //edit this for your column(s)
    //all of your columns will be added before the actions column
    $new_columns['MY_COLUMN_ID_1'] = 'Wykorzystany';
    $new_columns['MY_COLUMN_ID_2'] = 'Zamówienie';

    //stop editing
    $new_columns['order_actions'] = $columns['order_actions'];
    return $new_columns;
}

add_filter("manage_edit-register_code_sortable_columns", 'MY_COLUMNS_SORT_FUNCTION');
function MY_COLUMNS_SORT_FUNCTION($columns)
{
    $custom = array(
        'MY_COLUMN_ID_1' => 'is_used',
        'MY_COLUMN_ID_2' => 'order'
    );
    return wp_parse_args($custom, $columns);
}

add_action('manage_register_code_posts_custom_column', 'MY_COLUMNS_VALUES_FUNCTION', 2);
function MY_COLUMNS_VALUES_FUNCTION($column)
{
    global $post;
    $order = get_field('order', $post->ID);
    $is_used = get_field('is_used', $post->ID);
    //start editing, I was saving my fields for the orders as custom post meta

    if ($column == 'MY_COLUMN_ID_1') {
        if ($is_used) {
            echo 'TAK';
        } else {
            echo 'NIE';
        }
    }
    if ($column == 'MY_COLUMN_ID_2') {
        echo($order ? $order->ID : '');
    }
}
=======
>>>>>>> c2aced4cfedff6c4caf5bc2d0d20f056131491d3
