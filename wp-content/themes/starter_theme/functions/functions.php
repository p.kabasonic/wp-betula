<?php

function getOpinions($opinions)
{
    $new_opinions = array();
    $new_opinion = array();
    foreach ($opinions as $opinion) {
        $new_opinion['ID'] = $opinion->ID;
        $new_opinion['post_title'] = $opinion->post_title;
        $new_opinion['post_status'] = $opinion->post_status;
        $new_opinion['post_type'] = $opinion->post_type;
        $new_opinion['acf'] = get_fields($opinion->ID);
        array_push($new_opinions, $new_opinion);
    }

    return $new_opinions;
}

function getFaqs($faqs)
{
    $new_faqs = array();
    $new_faq = array();
    foreach ($faqs as $faq) {
        $new_faq['ID'] = $faq->ID;
        $new_faq['post_title'] = $faq->post_title;
        $new_faq['post_status'] = $faq->post_status;
        $new_faq['post_type'] = $faq->post_type;
        $new_faq['acf'] = get_fields($faq->ID);
        array_push($new_faqs, $new_faq);
    }

    return $new_faqs;
}

function getOffers($offers)
{
    $new_offers = array();
    $new_offer = array();
    foreach ($offers as $offer) {
        $new_offer['ID'] = $offer->ID;
        $new_offer['post_title'] = $offer->post_title;
        $new_offer['post_status'] = $offer->post_status;
        $new_offer['post_type'] = $offer->post_type;
        $new_offer['acf'] = get_fields($offer->ID);
        array_push($new_offers, $new_offer);
    }

    return $new_offers;
}

function getTeam($team)
{
    $new_team = array();
    $new_person = array();
    foreach ($team as $person) {
        $new_person['ID'] = $person->ID;
        $new_person['post_title'] = $person->post_title;
        $new_person['post_status'] = $person->post_status;
        $new_person['post_type'] = $person->post_type;
        $new_person['acf'] = get_fields($person->ID);

        array_push($new_team, $new_person);
    }

    return $new_team;
}

function getSimpleProduct($product)
{
    $prod_tmp = array();
    $typeMassageCategoryId = 39;

    $prod_tmp = $product->get_data();
    $price_excl = wc_get_price_excluding_tax($product); // price without VAT
    $price_incl = wc_get_price_excluding_tax($product);  // price included VAT
    $tax_amount = $price_incl - $price_excl; // VAT price amount
    $prod_tmp['price_excl'] = number_format($price_excl, DECIMALS, DECIMAL_SEPARATOR, THOUSANDS_SEPARATOR);
    $prod_tmp['tax_amount'] = number_format($tax_amount, DECIMALS, DECIMAL_SEPARATOR, THOUSANDS_SEPARATOR);
    $prod_tmp['tax_amount'] = number_format($tax_amount, DECIMALS, DECIMAL_SEPARATOR, THOUSANDS_SEPARATOR);
    $prod_tmp['attributes'] = array();
    $prod_tmp['in_stock'] = $product->is_in_stock();

    $images = array();
    foreach ($prod_tmp['gallery_image_ids'] as $key => $imageId):
        array_push($images, wp_get_attachment_url($imageId));
    endforeach;
    $prod_tmp['images'] = $images;
    $prod_tmp['image'] = get_the_post_thumbnail_url($product->get_id());

    $prod_tmp['variable'] = $product->is_type('variable');
    if ($prod_tmp['variable'])
        $prod_tmp['variations'] = $product->get_available_variations();

    $attributes = $product->get_attributes();
    foreach ($attributes as $attribute):
        $tmp = array();
        $tax = $attribute->get_taxonomy();
        $tmp['name'] = $tax;
        $tmp['label'] = wc_attribute_label($tax);
        $tmp['terms'] = $attribute->get_terms(); // The terms
        array_push($prod_tmp['attributes'], $tmp);
    endforeach;

    $prod_tmp['url'] = $product->get_permalink();
    $prod_tmp['acf'] = get_fields($product->get_id());
    $prod_tmp['rating'] = get_post_meta($product->get_id(), '_wc_average_rating', true);

    $cat = get_the_terms($prod_tmp['id'], 'product_cat');

    $productCategory = array();
    foreach ($cat as $category) {
        if ($category->parent == $typeMassageCategoryId) {
            array_push($productCategory, array(
                'name' => $category->name,
                'image' => wp_get_attachment_url(get_term_meta($category->term_id, 'thumbnail_id', true )),
            ));
        }
    }
    $prod_tmp['productCategory'] = $productCategory;

    return $prod_tmp;
}

function getProducts($products)
{
    $new_products = array();

    foreach ($products as $product) {
        $product = wc_get_product($product->ID);

        array_push($new_products, getSimpleProduct($product));
    }

    return $new_products;
}
