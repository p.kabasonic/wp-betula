<?php
if (!class_exists('Timber')) {
    echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
    return;
}

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

if (is_singular('product')) {
    $product = wc_get_product($context['post']->ID);

    $prod_tmp = getSimpleProduct($product);

    $terms = get_the_terms($post->ID, 'product_cat');
    if ($terms && !is_wp_error($terms)) {
        $context['category'] = $terms[0]->name;
        $context['category_url'] = get_term_link($terms[0]);

        $prod_tmp['categories'] = $terms;
    }

    $args = array(
        'taxonomy' => 'product_cat',
        'orderby' => 'name',
        'order' => 'asc',
        'hide_empty' => false,
        'parent' => 0
    );

    $prod_tmp['parentCategories'] = get_categories($args);

    if (isset($prod_tmp['acf']['masseurs']) && !empty($prod_tmp['acf']['masseurs'])) {
        $prod_tmp['acf']['masseurs'] = getTeam($prod_tmp['acf']['masseurs']);
    }

    $prod_tmp['is_packages'] = false;
    $prod_tmp['simple_variation'] = false;

    foreach($prod_tmp['variations'] as $variation ) {
        if (isset($variation['attributes'])) {
            if ($variation['attributes']['attribute_pa_pakage'] === 'five')
                $prod_tmp['is_packages'] = true;

            if ($variation['attributes']['attribute_pa_pakage'] === 'single' || $variation['attributes']['attribute_pa_pakage'] === 'five-people')
                $prod_tmp['simple_variation'] = true;
        }
    }

    $context['product'] = $prod_tmp;

    if (get_query_var('offer', false))
        $context['shopMode'] = true;
    else
        $context['shopMode'] = false;

    Timber::render('templates/woocommerce/single-product.twig', $context);
} else {
    $data = array();
    $args = array(
        'post_type' => 'offer',
        'posts_per_page' => -1,
    );

    $offers = get_posts($args);
    $data['offers'] = getOffers($offers);
    sort($data['offers']);

    $args = array(
        'taxonomy' => 'product_cat',
        'orderby' => 'name',
        'order' => 'asc',
        'hide_empty' => false,
        'parent' => 0
    );

    $parentCategories = get_categories($args);
    $tmp = array();

    foreach ($parentCategories as $cat) {
        $cat->value = true;

        if ($cat->term_id !== 15)
            array_push($tmp, $cat);
    }
    $data['productParentCategories'] = $tmp;
    sort($data['productParentCategories']);

    $args = array(
        'taxonomy' => 'product_cat',
        'orderby' => 'name',
        'order' => 'asc',
        'hide_empty' => false,
    );
    $categories = get_categories($args);

    $tmp = array();
    foreach ($categories as $cat) {
        $cat->value = false;

        array_push($tmp, $cat);
    }

    $data['productCategories'] = $tmp;

    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => -1,
        'suppress_filters' => false,
    );

    $products = get_posts($args);

    $data['countProducts'] = count($products);

    $data['products'] = getProducts($products);
    $context['data'] = $data;

    Timber::render('templates/woocommerce/archive.twig', $context);
}
