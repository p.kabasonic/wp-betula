<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
    exit;
}

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
    echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
    return;
}
global $woocommerce;
$cart_products = getProductsCart($woocommerce->cart->get_cart());
$countCartProducts = count($cart_products);
?>
<div class="checkout-page">
    <div class="section">
        <div class="container-fluid">
            <div class="wrapper">
                <h2 data-aos="fade" class="h2 bottom-green-line section__header">Oferta</h2>
                <div data-aos="fade" class="shop-header bottom-silver-line">
                    <div class="breadcrumbs"><a href="<?php echo get_home_url(); ?>" title="Strona główna">Strona główna</a><span class="slash">/</span>
                        <a href="<?php echo get_permalink(wc_get_page_id('shop'))."?tab=2"; ?>" title="Dla bliskich">Dla bliskich</a>
                        <span class="d-none d-md-inline-flex">
                            <span class="slash">/</span><a href="<?php echo wc_get_cart_url(); ?>" title="Koszyk">Koszyk</a>
                        <span class="slash">/</span>Płatność
                        </span>
                    </div>
                    <a href="<?php echo wc_get_cart_url(); ?>" class="cart" title="Koszyk">
                        <svg id="Group_5070" data-name="Group 5070" xmlns="http://www.w3.org/2000/svg" width="26.45"
                             height="23"
                             viewBox="0 0 26.45 23">
                            <path id="Path_4928" data-name="Path 4928"
                                  d="M4.862,10a.863.863,0,0,0,0,1.725H8.3c1.427,3.535,2.834,7.077,4.25,10.62l-1.3,3.135a.863.863,0,0,0,.8,1.195H26.425a.863.863,0,1,0,0-1.725H13.344l.746-1.779,13.845-1.105a.887.887,0,0,0,.773-.665l1.725-7.475a.9.9,0,0,0-.845-1.051H10.622l-.934-2.336a.884.884,0,0,0-.8-.539Zm6.451,4.6H28.5l-1.339,5.8L14.054,21.446Zm3.612,12.65A2.875,2.875,0,1,0,17.8,30.125,2.888,2.888,0,0,0,14.925,27.25Zm8.625,0a2.875,2.875,0,1,0,2.875,2.875A2.888,2.888,0,0,0,23.55,27.25Zm-8.625,1.725a1.15,1.15,0,1,1-1.15,1.15A1.137,1.137,0,0,1,14.925,28.975Zm8.625,0a1.15,1.15,0,1,1-1.15,1.15A1.137,1.137,0,0,1,23.55,28.975Z"
                                  transform="translate(-4 -10)" fill="#8bc549"/>
                        </svg>
                        Koszyk (<span class="js-count-cart-products"><?php echo $countCartProducts; ?></span>)
                    </a>
                </div>
                <div data-aos="fade" class="section__content">
                    <h1 class="h2 section__title">
                        Płatność
                    </h1>
                    <form
                        name="checkout"
                        method="post"
                        class="checkout woocommerce-checkout"
                        action="<?php echo esc_url(wc_get_checkout_url()); ?>"
                        enctype="multipart/form-data"
                    >

                        <?php if ($checkout->get_checkout_fields()) : ?>

                            <?php do_action('woocommerce_checkout_before_customer_details'); ?>

                            <div class="col2-set" id="customer_details">
                                <?php do_action('woocommerce_checkout_billing'); ?>
                                <?php do_action('woocommerce_checkout_shipping'); ?>
                            </div>

                            <?php do_action('woocommerce_checkout_after_customer_details'); ?>

                        <?php endif; ?>

                        <?php do_action('woocommerce_checkout_before_order_review_heading'); ?>

                        <div class="d-flex justify-content-end top-light-green-line">
                            <div class="section__review">
                                <h3 id="order_review_heading" class="h3 order_review__title">Do zapłaty</h3>

                                <?php do_action('woocommerce_checkout_before_order_review'); ?>

                                <div id="order_review" class="woocommerce-checkout-review-order">
                                    <?php do_action('woocommerce_checkout_order_review'); ?>
                                </div>
                            </div>
                        </div>

                        <?php do_action('woocommerce_checkout_after_order_review'); ?>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>
