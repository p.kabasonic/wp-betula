<?php
/**
 * Template Name: Team
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

if (isset($context['acf']['team']) && !empty($context['acf']['team'])) {
    $context['acf']['team'] = getTeam($context['acf']['team']);
}

Timber::render('team.twig', $context);
