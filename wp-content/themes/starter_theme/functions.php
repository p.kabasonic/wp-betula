<?php

if (!class_exists('Timber')) {
    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });

    add_filter('template_include', function ($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite
{

    function __construct()
    {
        add_theme_support('post-formats', array());
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
        add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'taxonomies', 'category'));
        add_filter('timber_context', array($this, 'add_to_context'));
        add_filter('get_twig', array($this, 'add_to_twig'));
        add_action('init', array($this, 'register_post_types'));
        add_action('init', array($this, 'register_taxonomies'));
        add_action('rest_api_init', array($this, 'register_rest_routes'));
        parent::__construct();
    }

    function register_post_types()
    {
        register_post_type('opinion',
            array(
                'labels' => array(
                    'name' => __('Opinie'),
                    'singular_name' => __('Opinia')
                ),
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => false, 'with_front' => false),
                'publicly_queryable' => false,
                'supports' => array('title', 'comment-list', 'comment-form', 'search-form', 'caption'),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => true,
                'menu_icon' => 'dashicons-awards'
            )
        );
        register_post_type('faq',
            array(
                'labels' => array(
                    'name' => __('FAQ'),
                    'singular_name' => __('FAQ')
                ),
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => false, 'with_front' => false),
                'publicly_queryable' => false,
                'supports' => array('title', 'comment-list', 'comment-form', 'search-form', 'caption'),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => true,
                'menu_icon' => 'dashicons-format-status'
            )
        );
        register_post_type('team',
            array(
                'labels' => array(
                    'name' => __('Zespol'),
                    'singular_name' => __('Zespol')
                ),
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => false, 'with_front' => false),
                'publicly_queryable' => false,
                'supports' => array('title', 'comment-list', 'comment-form', 'search-form', 'caption'),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => true,
                'menu_icon' => 'dashicons-id-alt'
            )
        );
        register_post_type('offer',
            array(
                'labels' => array(
                    'name' => __('Oferty'),
                    'singular_name' => __('Oferta')
                ),
                'public' => true,
                'has_archive' => false,
                'rewrite' => array('slug' => false, 'with_front' => false),
                'publicly_queryable' => false,
                'supports' => array('title', 'comment-list', 'comment-form', 'search-form', 'caption'),
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'show_in_rest' => true,
                'menu_icon' => 'dashicons-id-alt'
            )
        );
        register_post_type('register_code',
            array(
                'labels' => array(
                    'name' => __('Kupon'),
                    'singular_name' => __('Kupon')
                ),
                'public' => true,
                'has_archive' => true,
                'rewrite' => array('slug' => 'code'),
                'publicly_queryable' => true,
                'description' => 'code',
                'menu_icon' => 'dashicons-groups',
                'supports' => array('search-form', 'title', 'editor', 'custom-fields', 'page-attributes', 'thumbnail'),
            )
        );
    }

    function register_taxonomies()
    {
    }

    function register_nav()
    {
        register_nav_menu('primary', __('Primary Menu', 'top-menu'));
    }

    function add_to_context($context)
    {
        if (is_array(get_fields())) {
            foreach (get_fields() as $key => $field) {
                if (count((array)$field) && is_array($field)) {
                    $layouts = array_filter($field, function ($item) {
                        if (!is_array($item)) return false;
                        return array_key_exists('acf_fc_layout', $item);
                    });
                    if (count($layouts)) {
                        foreach ($layouts as $index => $layout) {
                            $context['layouts'][$index] = $layout;

                        }
                    }
                }
            }
        }
        $context['title'] = get_the_title(get_the_ID());
        $context['postID'] = get_the_ID();

        if (get_the_ID())
            $context['acf'] = get_fields(get_the_ID());


        if (is_home() || is_front_page()) {
            $context['home'] = true;
        } else {
            $context['home'] = false;
        }
        $context['links']['home_url'] = get_home_url();
        $context['links']['checkout_url'] = wc_get_checkout_url();
        $context['links']['cart_url'] = wc_get_cart_url();
        $context['links']['shop_url'] = get_permalink(wc_get_page_id('shop'));
        $context['menu'] = new TimberMenu();
        $context['cart'] = getCart();
        $context['site'] = $this;
        $context['options'] = get_fields('options');
        $context['template_url'] = get_template_directory_uri();
        return $context;
    }

    function myfoo($text)
    {
        $text .= ' bar!';
        return $text;
    }

    function add_to_twig($twig)
    {
        /* this is where you can add your own functions to twig */
        $twig->addExtension(new Twig_Extension_StringLoader());
        $twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
        return $twig;
    }

}


new StarterSite();


$composer_autoload = __DIR__ . '/vendor/autoload.php';
require_once($composer_autoload);

function theme_styles()
{
    wp_enqueue_style('main_css', get_template_directory_uri() . '/dist/main.css', array(), '1.1.11');
}

add_action('wp_enqueue_scripts', 'theme_styles');

function theme_js()
{
    global $wp_scripts;
    wp_enqueue_script('my_custom_js', get_template_directory_uri() . '/dist/main.js', array(), '1.1.11', true);
    wp_enqueue_script('vue_js', get_template_directory_uri() . '/dist/vue.js', array(), '1.1.11', true);

    wp_localize_script('my_custom_js', 'dynamic_scripts',
        array('theme_url' => get_template_directory_uri(),
            'ajax' => admin_url('admin-ajax.php'),
        )
    );
}

add_action('wp_enqueue_scripts', 'theme_js');

add_action('wp', 'create_layout_file_if_not_exist');
function create_layout_file_if_not_exist()
{
    if (is_admin()) return;
    if (is_array(get_fields())) {
        foreach (get_fields() as $flex) {
            if (is_array($flex)) {
                foreach ($flex as $flexEl) {
                    if (is_array($flexEl) && array_key_exists('acf_fc_layout', $flexEl)) {
                        $layout_dir = get_template_directory() . '/templates/layouts/';
                        $filename = $layout_dir . $flexEl['acf_fc_layout'] . '.twig';
                        if (!file_exists($filename)) {
                            copy($layout_dir . 'layout-default.twig', $filename); //kopiujemy plik z defaut
                            exec('chmod 0666 ' . $filename);
                            chmod($filename, 0666);
                        }
                    }
                }
            }
        }
    }
}

//includes additional functions

require get_template_directory() . '/include/sec.php';
require get_template_directory() . '/functions/shop.php';
require get_template_directory() . '/functions/functions.php';


if (function_exists('acf_add_options_page')) {

    // add parent
    $parent = acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme General Settings',
        'menu_slug' => 'theme-option',
        'position' => 25,
        'redirect' => true
    ));

    // add sub page
    acf_add_options_sub_page(array(
        'page_title' => 'General Settings',
        'menu_title' => 'General Settings',
        'menu_slug' => 'general',
        'parent_slug' => 'theme-option',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Footer Section',
        'menu_title' => 'Footer Section',
        'menu_slug' => 'footer',
        'parent_slug' => 'theme-option',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Sub Footer Section',
        'menu_title' => 'Sub Footer Section',
        'menu_slug' => 'sub-footer',
        'parent_slug' => 'theme-option',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Oferty',
        'menu_title' => 'Oferty',
        'menu_slug' => 'offers',
        'parent_slug' => 'theme-option',
    ));

}


//export acf fields
add_filter('acf/settings/save_json', 'ks_acf_json_save_point');
function ks_acf_json_save_point($path)
{
    $path = get_template_directory() . '/acf-json';
    return $path;
}

//import acf fields
add_filter('acf/settings/load_json', 'ks_acf_json_load_point');
function ks_acf_json_load_point($paths)
{
    unset($paths[0]);
    $paths[] = get_template_directory() . '/acf-json';
    return $paths;
}


function my_custom_mime_types($mimes)
{

// New allowed mime types.
    $mimes['svg'] = 'image/svg+xml';
//    $mimes['svgz'] = 'image/svg+xml';
//    $mimes['doc'] = 'application/msword';

// Optional. Remove a mime type.
    unset($mimes['exe']);

    return $mimes;
}

add_filter('upload_mimes', 'my_custom_mime_types');

add_action('after_setup_theme', 'mytheme_add_woocommerce_support');
function mytheme_add_woocommerce_support()
{
    add_theme_support('woocommerce');
}


//Disable the default stylesheet woocommerce
add_filter('woocommerce_enqueue_styles', '__return_empty_array');


add_filter('query_vars', 'add_query_vars_filter');
function add_query_vars_filter($vars)
{
    $vars[] = "offer";
    return $vars;
}

