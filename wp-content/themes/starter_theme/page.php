<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

//dump($context);
//    die;

if ($context['acf'] && $context['acf']['sections']) {
    $count = count($context['acf']['sections']);

    for ($i = 0; $i < $count; $i++) {
        if (isset($context['acf']['sections'][$i]['faqs']) && !empty($context['acf']['sections'][$i]['faqs'])) {
            $context['layouts'][$i]['faqs'] = getFaqs($context['acf']['sections'][$i]['faqs']);
        }

        if (isset($context['acf']['sections'][$i]['opinions']) && !empty($context['acf']['sections'][$i]['opinions'])) {
            $context['layouts'][$i]['opinions'] = getOpinions($context['acf']['sections'][$i]['opinions']);
        }

        if (isset($context['acf']['sections'][$i]['offers']) && !empty($context['acf']['sections'][$i]['offers'])) {
            $context['layouts'][$i]['offers'] = getOffers($context['acf']['sections'][$i]['offers']);
        }
    }
}

if (is_cart()) {
    Timber::render(array('woocommerce/cart.twig'), $context);
    return;
}

if (is_checkout() && !empty(is_wc_endpoint_url('order-received'))) {
    Timber::render(array('woocommerce/thank-you-page.twig'), $context);
    return;
}

Timber::render(array('page' . $post->post_name . '.twig', 'page.twig'), $context);
