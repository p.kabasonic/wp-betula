<?php

add_action('customize_register', 'mytheme_customizer_options');
function mytheme_customizer_options($wp_customize)
{
    $wp_customize->add_setting( 'logo_reverse' ); // Add setting for logo uploader
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_reverse', array(
        'label' => __('Upload Logo reverse'),
        'section' => 'title_tagline',
        'priority'   => 8,
        'settings' => 'logo_reverse',
    )));
}
