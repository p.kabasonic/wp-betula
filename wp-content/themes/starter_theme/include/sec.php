<?php
//Disable edit file on admin side
define('DISALLOW_FILE_EDIT', true);

//disable xmlrpc
add_filter('xmlrpc_enabled', '__return_false');

// Filters for WP-API version 1.x
add_filter('json_enabled', '__return_false');
add_filter('json_jsonp_enabled', '__return_false');

// Filters for WP-API version 2.x
//add_filter( 'rest_enabled', '__return_false' );
//add_filter( 'rest_jsonp_enabled', '__return_false' );

// Remove REST API info from head and headers
remove_action('xmlrpc_rsd_apis', 'rest_output_rsd');
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('template_redirect', 'rest_output_link_header', 11);

//remove usr rest api
add_filter('rest_endpoints', function ($endpoints) {
    if (isset($endpoints['/wp/v2/users'])) {
        unset($endpoints['/wp/v2/users']);
    }
    if (isset($endpoints['/wp/v2/users/(?P<id>[\d]+)'])) {
        unset($endpoints['/wp/v2/users/(?P<id>[\d]+)']);
    }
    return $endpoints;
});

// Hide version i head
remove_action('wp_head', 'wp_generator');

// Hide version in RSS channel
function my_secure_generator($generator, $type)
{
    return '';
}

add_filter('the_generator', 'my_secure_generator', 10, 2);

//Hide version in scripts
function my_remove_src_version($src)
{
    global $wp_version;
    $version_str = '?ver=' . $wp_version;
    $offset = strlen($src) - strlen($version_str);
    if ($offset >= 0 && strpos($src, $version_str, $offset) !== FALSE) return substr($src, 0, $offset);
    return $src;
}
add_filter( 'script_loader_src', 'my_remove_src_version' );
add_filter( 'style_loader_src', 'my_remove_src_version' );
