import jQuery from 'jquery'
import Swiper, {Navigation} from 'swiper';
import AOS from 'aos';

Swiper.use([Navigation]);

import bootstrap from 'bootstrap/dist/js/bootstrap.min'
import 'swiper/swiper.scss';
import style from './styles/index.scss'

const opinnionCrousel = () => {
  let slidesPerView = 1
  let slidesPerGroup = 1

  if (window.innerWidth > 768 && window.innerWidth < 1023) {
    slidesPerView = 2
    slidesPerGroup = 2
  }

  if (window.innerWidth > 1024) {
    slidesPerView = 3
    slidesPerGroup = 3
  }

  const currentSlideEl = document.getElementById('js-current-slide')
  const countGroupEl = document.getElementById('js-count-group')
  const opinionsCarousel = document.getElementById('js-opinions-carousel')

  if (opinionsCarousel) {
    const countOptions = opinionsCarousel.dataset.count
    var countGroup = Math.ceil(parseInt(countOptions) / slidesPerView) || 1

    if (countGroupEl)
      countGroupEl.innerText = `${countGroup}`
  }

  const swiper = new Swiper(opinionsCarousel, {
    spaceBetween: 14,
    slidesPerView: slidesPerView,
    slidesPerGroup: slidesPerGroup,
    allowTouchMove: true,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
      nextEl: '.swiper-next',
      prevEl: '.swiper-prev',
    },
    breakpoints: {
      768: {
        spaceBetween: 24,
        slidesPerView: slidesPerView,
        slidesPerGroup: slidesPerGroup,
        allowTouchMove: true,
      },
      1024: {
        spaceBetween: 32,
        slidesPerView: slidesPerView,
        slidesPerGroup: slidesPerGroup,
        allowTouchMove: false,
      },
    },
  });

  swiper.on('slideChangeTransitionStart', function (swipe) {
    currentSlideEl.innerText = geSlideDataIndex(swipe, slidesPerGroup) + 1
  });

  function geSlideDataIndex(swipe, slidesPerGroup) {
    var activeIndex = swipe.activeIndex;
    var slidesLen = swipe.slides.length;

    if (slidesPerGroup > 1) {
      activeIndex = swipe.activeIndex / slidesPerGroup
      slidesLen = swipe.slides.length / slidesPerGroup
    }

    if (swipe.params.loop) {
      switch (activeIndex) {
        case 0:
          activeIndex = slidesLen - 3;
          break;
        case slidesLen - 1:
          activeIndex = 0;
          break;
        default:
          --activeIndex;
      }
    }
    return activeIndex;
  }
}

window.addEventListener('load', opinnionCrousel)

jQuery(function () {
  AOS.init(
    {
      disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
      startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
      initClassName: 'aos-init', // class applied after initialization
      animatedClassName: 'aos-animate', // class applied on animation
      useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
      disableMutationObserver: false, // disables automatic mutations' detections (advanced)
      debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
      throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)

      // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
      offset: 100, // offset (in px) from the original trigger point
      delay: 0, // values from 0 to 3000, with step 50ms
      duration: 400, // values from 0 to 3000, with step 50ms
      easing: 'ease', // default easing for AOS animations
      once: false, // whether animation should happen only once - while scrolling down
      mirror: false, // whether elements should animate out while scrolling past them
      anchorPlacement: 'top-bottom', // defines which position of the element regarding to window
    }
  );
})


jQuery(document).ready(function ($) {
  window.addEventListener('load', AOS.refresh)

  $('.js-hamburger').click(function () {
    $(this).toggleClass('open')
    $('.navigation-mobile').toggleClass('open')
  })

  $('.js-nav-item').click(function () {
    if ($('.js-hamburger').hasClass('open')) {
      $('.js-hamburger').toggleClass('open')
      $('.navigation-mobile').toggleClass('open')
    }
  })

  $('.js-filter-mobile').click(function () {
    $('.js-filter-content').addClass('open')
    $('.js-filter-close').addClass('open')
    $('body').addClass('overflow-hidden')
  })

  $('.js-filter-close').click(function () {
    $('.js-filter-content').removeClass('open')
    $(this).removeClass('open')
    $('body').removeClass('overflow-hidden')
  })

  $('.js-button-booksy').click(function () {
    $('.booksy-widget-button').trigger('click');
  })

  $('#js-map-click').click(function () {
    if ($(this).data('link') && $(this).data('link') !== '')
      window.open($(this).data('link'), '_blank');
  })

  $('.js-contact').click(function (e) {
    e.preventDefault()

    window.scrollTo(0, document.body.scrollHeight);
  })

  $('#js-checkout-invoice').change(function () {
    $('#billing_address_1_field').slideToggle();
    $('#billing_address_2_field').slideToggle();
    $('#billing_postcode_field').slideToggle();
    $('#billing_phone_field').slideToggle();
    $('#billing_city_field').slideToggle();
  });

  $('.js-select-label').click(function () {
    $(this).parent().toggleClass('open')
  })

  $('.js-select-item').click(function () {
    $(this).parent().parent().removeClass('open')
  })

  $(document).on("click", function (event) {
    let lastEvent
    let trigger = true
    var $trigger = $('.js-select')

    if (trigger) {
      lastEvent = event.target
      trigger = false
    }

    if ($trigger !== event.target && !$trigger.has(event.target).length) {
      lastEvent = event.target
      console.log('sdvsdvdsdv')

      $(".js-select.open").removeClass('open')
      trigger = true
    }
  });
})
