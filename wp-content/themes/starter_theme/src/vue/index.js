import Vue from 'vue'
import VueRouter from 'vue-router'

import store from "~/store"

import Default from "~/components/Default";
import OfferPage from "~/components/OfferPage";
import CTA from "~/components/CTA";
import PricePage from "~/components/PricePage";
import CartPage from "~/components/CartPage";

import axios from "axios";

Vue.config.productionTip = process.env.NODE_ENV === 'production'
if (process.env.NODE_ENV === 'production') {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
}

Vue.prototype.$axios = axios
Vue.use(VueRouter)

let router = new VueRouter({
  mode: 'history',
  routes: []
})

new Vue({
  el: '#vue',
  store,
  router,
  components: {
    Default,
    OfferPage,
    CTA,
    PricePage,
    CartPage,
  },
})
