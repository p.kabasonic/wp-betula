import {updateField} from "vuex-map-fields";

export default {
  updateField,
  setCart: (state, payload) => {
    state.cart = payload
  },
}
