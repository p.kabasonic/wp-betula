import request from '~/api/request'

export default  {
  ['AJAX_GET_PRODUCTS_BY_CATEGORY']({state, commit}, payload) {
    let params = new URLSearchParams();
    params.append('action', 'get_products_by_category');
    params.append('ids', payload.ids);
    params.append('promotion', payload.promotion);

    return new Promise((resolve, reject) => {
      request.post('/wp-admin/admin-ajax.php', params)
        .then(response => {
          resolve(response.data);
        })
        .catch(error => reject(error))
    })
  },
  ['AJAX_ADD_TO_CART']({state, commit}, payload) {
    let params = new URLSearchParams();
    params.append('action', 'add_to_cart');
    params.append('id', payload.id);
    params.append('quantity', payload.quantity);

    if (payload.attributes)
      for (const [key, value] of Object.entries(payload.attributes)) {
        params.append(`attributes[${key}][type]`, key);
        params.append(`attributes[${key}][value]`, value);
      }

    return new Promise((resolve, reject) => {
      request.post('/wp-admin/admin-ajax.php', params)
        .then(response => {
          if (response.data.success)
            commit('setCart', response.data.data.cart)

          resolve(response.data);
        })
        .catch(error => reject(error))
    })
  },
  ['AJAX_GET_CART']({commit}) {
    let params = new URLSearchParams();
    params.append('action', 'get_cart');

    return new Promise((resolve, reject) => {
      request.post('/wp-admin/admin-ajax.php', params)
        .then(response => {
          if (response.data.success)
            commit('setCart', response.data.data)

          resolve(response.data);
        })
        .catch(error => reject(error))
    })
  },
  ['AJAX_REMOVE_FROM_CART']({commit}, payload) {
    let params = new URLSearchParams();
    params.append('action', 'remove_from_cart');
    params.append('cart_item_key', payload);

    return new Promise((resolve, reject) => {
      request.post('/wp-admin/admin-ajax.php', params)
        .then(response => {
          if (response.data.success)
            commit('setCart', response.data.data.cart)

          resolve(response.data);
        })
        .catch(error => reject(error))
    })
  },
  ['AJAX_SET_QUANTITY']({commit}, payload) {
    let params = new URLSearchParams();
    params.append('action', 'set_quantity');
    params.append('cart_item_key', payload.key);
    params.append('quantity', payload.quantity);

    return new Promise((resolve, reject) => {
      request.post('/wp-admin/admin-ajax.php', params)
        .then(response => {
          if (response.data.success)
            commit('setCart', response.data.data.cart)

          resolve(response.data);
        })
        .catch(error => reject(error))
    })
  },
  ['AJAX_ADD_COUPON_CODE']({commit}, payload) {
    let params = new URLSearchParams();
    params.append('action', 'check_coupon_code');
    params.append('coupon', payload.coupon);

    return new Promise((resolve, reject) => {
      request.post('/wp-admin/admin-ajax.php', params)
        .then(response => {
          if (response.data.success)
            commit('setCart', response.data.data.cart)

          resolve(response.data);
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  ['REMOVE_COUPON_CODE']({commit}) {
    let params = new URLSearchParams();
    params.append('action', 'remove_coupon_code');

    return new Promise((resolve, reject) => {
      request.post('/wp-admin/admin-ajax.php', params)
        .then(response => {
          if (response.data.success) {
            commit('setCart', response.data.data.cart)
          }

          resolve(response.data);
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}
