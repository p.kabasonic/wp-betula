<?php
/**
 * Template Name: Price list
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$args = array(
    'taxonomy' => 'product_cat',
    'orderby' => 'name',
    'order' => 'asc',
    'hide_empty' => false,
);

$categories = get_categories($args);
$data['productCategories'] = $categories;

$tmp = array();
foreach ($categories as $cat) {
    if ($cat->parent === 39)
        array_push($tmp, $cat);
}
$data['categories'] = $tmp;
sort($data['categories']);

$productsByCategory = array();

$args = array(
    'post_type' => 'product',
    'orderby' => 'name',
    'posts_per_page' => -1,
    'tax_query' => array(
        'relation' => 'AND',
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => null,
        )
    )
);

foreach ($data['categories'] as $cat) {
    $args['tax_query'][0]['terms'] = $cat->term_id;

    array_push($productsByCategory, getProducts(get_posts($args)));
}

$args = array(
    'post_type' => 'product',
    'orderby' => 'name',
    'posts_per_page' => -1,
);

$data['products'] = getProducts(get_posts($args));

$data['productsByCategory'] = $productsByCategory;
$context['data'] = $data;

Timber::render('price-list.twig', $context);
