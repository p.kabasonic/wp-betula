<?php
/**
 * Template Name: Promotions
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

if ($context['acf'] && isset($context['acf']['promotion']) && $context['acf']['promotion']) {
    for ($i = 0; $i < count($context['acf']['promotion']); $i++) {
        $context['acf']['promotion'][$i]['product'] = get_post_permalink($context['acf']['promotion'][$i]['product'][0]->ID);
    }
}

Timber::render('promotions.twig', $context);
